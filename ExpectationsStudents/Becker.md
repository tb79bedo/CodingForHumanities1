# Markdown File
### Describing my expectations about this course

I hope I will be able to manage small skills in coding. I think I definitely will sit many hours in front of a screen. And perhaps it will be a little bit of fun, as well. 

[So here's a short video](https://www.youtube.com/watch?v=v-o2CcUjIP0)

#### Tasks I accomplished in this Markdown File

* Produce a markdown file
* Describe my expectations about this course
* Two headers of different size
* A hyperlink

* Oh yes and a bullet point list

